# -*- coding: utf-8 -*-

from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from models import Poll


class PollTest(TestCase):

    def setUp(self):
        self.expected_question = "Jakie jest pytanie?"
        self.expected_choice = "Czy lubisz precelki?"
        self.poll = Poll.objects.create(
            question=self.expected_question,
            pub_date=timezone.now())
        self.choice = self.poll.choice_set.create(
            choice=self.expected_choice)

    def test_poll_display(self):
        self.assertEquals(unicode(self.poll), self.expected_question)
        new_question = u"Jaka jest odpowiedź?"
        self.poll.question = new_question
        self.assertEquals(unicode(self.poll), new_question)

    def test_choice_display(self):
        self.assertEquals(unicode(self.choice), self.expected_choice)
        new_choice = "Czy lewa strona jest lepsza od prawej?"
        self.choice.choice = new_choice
        self.assertEquals(unicode(self.choice), new_choice)

    def test_published_today(self):
        self.assertTrue(self.poll.published_today())
        delta = timedelta(hours=26)
        self.poll.pub_date = self.poll.pub_date - delta
        self.assertFalse(self.poll.published_today())